package model;

import java.io.Serializable;

public final class CustomWrapper implements Serializable {

	private static final long serialVersionUID = 4866942005406638979L;

	String id, result, name;
	double value;
	String idJs, resultJs;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getIdJs() {
		return idJs;
	}

	public String getResultJs() {
		return resultJs;
	}

	public void setIdJs(String idJs) {
		this.idJs = idJs;
	}

	public void setResultJs(String resultJs) {
		this.resultJs = resultJs;
	}

	public CustomWrapper(String id, String result, String name, double value) {
		super();
		this.id = id;
		this.result = result;
		this.name = name + " * " + value;
		this.value = value;
		this.idJs = "#" + id;
		this.resultJs = "#" + result;
	}
}
