package service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import model.CustomWrapper;

@Service
public class MainService {

	
	
   public List<CustomWrapper> getDefList()
    {
    	List<CustomWrapper> list= new ArrayList<CustomWrapper>();
    	list.add(new CustomWrapper("text2", "result2", "Archer", 100));
    	return list;
    }
    
   public List<CustomWrapper> getList()
    {
    	List<CustomWrapper> list= new ArrayList<CustomWrapper>();
    	list.add(new CustomWrapper("text1", "result1", "Pikeman", 240));

    	list.add(new CustomWrapper("textP", "resultP", "Paladin", 350));

    	list.add(new CustomWrapper("textPt", "resultPt", "Pathfinder", 570));
    	list.add(new CustomWrapper("textK", "resultK", "Knight", 2100));
    	list.add(new CustomWrapper("textG", "resultG", "Great Lord", 3150));
    	list.add(new CustomWrapper("textR", "resultR", "Reaver", 3500));
    	return list;
    } 
}
