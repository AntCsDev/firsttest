package service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import model.CustomWrapper;

@Service
public class InputService {

	private static final String splitBy = "\t";

	private final List<CustomWrapper> listDefensive = new ArrayList<CustomWrapper>();
	private final List<CustomWrapper> listOffensive = new ArrayList<CustomWrapper>();

	/**
	 * Only way to access file inside a JAR.
	 * JDK 1.8 + Required
	 * Will fetch the input file 
	 * and will load the line 
	 */
	@PostConstruct
	public void loadFile() {
		InputStream is = getClass().getClassLoader().getResourceAsStream("file/input.txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		int count = 0;
		for (String line : reader.lines().collect(Collectors.toList())) {
			addToList(line.split(splitBy), count);
			count++;
		}
	}

	public List<CustomWrapper> getListDefensive() {
		return listDefensive;
	}

	public List<CustomWrapper> getListOffensive() {
		return listOffensive;
	}

	/**
	 * False means Def, True Off
	 */
	private static boolean flag = false;

	private void addToList(String arr[], int count) {
		if (arr[0].equals("Title") && arr[1].contains("Offe"))
			flag = true;
		else if (arr[0].equals("Title") && arr[1].contains("Defen"))
			flag = false;
		else if (flag)
			listOffensive.add(getInstance(arr, count));
		else
			listDefensive.add(getInstance(arr, count));
	}

	private CustomWrapper getInstance(String arr[], int count) {
		return new CustomWrapper("textIn" + count, "resultIn" + count, arr[0], Double.parseDouble(arr[1]));
	}
}
