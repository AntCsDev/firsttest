package main;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import service.InputService;

@Controller
public class MainContoller {

	@Autowired
	private InputService mainService;
	
    @RequestMapping({"/*","/home"})
    public String home( Model model,HttpServletRequest httpRequest) {
    	System.out.println(getClientIp(httpRequest));
        model.addAttribute("offList", mainService.getListOffensive());
        model.addAttribute("defList", mainService.getListDefensive());
        return "main";
    }
    
    private static String getClientIp(HttpServletRequest request) {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        return remoteAddr;
    }
    
    

}
